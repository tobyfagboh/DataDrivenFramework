package com.w2a.testcase;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.apache.log4j.Logger;

import com.w2a.base.TestBase;

public class loginTest extends TestBase{

	@Test
	public void loginAsAdmin() throws InterruptedException {
		Logger log = Logger.getLogger("devpinoyLogger");
		log.debug("Inside login test");
		driver.findElement(By.name(OR.getProperty("emailName"))).sendKeys("admin@indigo.com");
		driver.findElement(By.name(OR.getProperty("passwordName"))).sendKeys("password");
		driver.findElement(By.xpath(OR.getProperty("loginBtnXpath"))).click();
		Assert.assertTrue(isElementPresent(By.xpath(OR.getProperty("searchForPatientXpath"))),"Login not successful");
		Thread.sleep(3000);
		log.debug("Login successfully executed");
	}
}