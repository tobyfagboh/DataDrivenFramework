package com.w2a.testcase;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.w2a.base.TestBase;



public class addPatientTest extends TestBase{
	
	@Test (dataProvider="getData")
	public void addPatient(String firstName, String lastName, String phoneNumber, String Email) throws InterruptedException {
		driver.findElement(By.xpath(OR.getProperty("patientRegBtnXpath"))).click();
		driver.findElement(By.xpath(OR.getProperty("RegPatientXpath"))).click();
		driver.findElement(By.id(OR.getProperty("firstname"))).sendKeys("firstName");
		driver.findElement(By.id(OR.getProperty("lastname"))).sendKeys("lastName");
		driver.findElement(By.id(OR.getProperty("phonenumber"))).sendKeys("phoneNumber");
		driver.findElement(By.name(OR.getProperty("email"))).sendKeys("Email");
		Thread.sleep(3000);
	}
	
	@DataProvider
	public Object[][] getData(){
		
		String sheetName = "addPatientTest";
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);
		
		Object[][] data = new Object[rows-1][cols];
		
		for (int rowNum = 2; rowNum <= rows; rowNum++) {
			for (int colNum = 0; colNum < cols; colNum++) {
				
				data[rowNum - 2][colNum] = excel.getCellData(sheetName, colNum, rowNum);
				
			}
		}
		
		return data;
	}

}
